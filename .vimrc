"        _ _                              _
"    ___| (_)___  ___ _ __   ___ ___   __| | ___
"   / _ \ | / __|/ _ \ '_ \ / __/ _ \ / _` |/ _ \
"  |  __/ | \__ \  __/ | | | (_| (_) | (_| |  __/
"   \___|_|_|___/\___|_| |_|\___\___/ \__,_|\___|

"=================== ELISE CONFIGURATION BEGIN ====================="
"
" configurer Vim pour utiliser UTF-8 en interne
" Encoding used or the keyboard and display. It is also the default encoding
" for files.
set encoding=utf-8

" vim doesn't show the command that is being typed:
" https://vi.stackexchange.com/questions/6307/vim-doesnt-show-the-command-that-is-being-typed
" You must have set showcmd after set no compatible
set nocompatible
set showcmd        " vim show the command that is being typed

"====== Turn Off Swap Files
set noswapfile    " http://robots.thoughtbot.com/post/18739402579/global-gitignore#comment-458413287
set nobackup
set nowb          " nowritebackup
"======

"====== Vim folding settings
" https://github.com/jordanhudgens/vim-settings/blob/master/vimrc

set foldmethod=manual

augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    augroup END

    nnoremap <silent> <Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
    vnoremap <Space> zf

"======

" Autocommands{{{}}}
"
augroup filetype_autocompletes
    autocmd!
    autocmd FileType python :iabbrev <buffer> iff if:<left>
    autocmd FileType javascript :iabbrev <buffer> iff if ()<left>
augroup END

" Operator pending mappings
"
"" Run cin( to delete and start typing in the next parens on that line
onoremap in( :<c-u>normal! f(vi(<cr>
onoremap in{ :<c-u>normal! f{vi{<cr>


" Tells Vim to use your system clipboard for copy/paste/cut
" https://dbo1093.medium.com/vim-for-developers-part-4-custom-configurations-7f1db2f1ef3d
set clipboard=unnamed

" For mouse click
:set mouse=a

" Show absolute line numbers
set nu
" relative line numbers
set rnu

set ruler       " show the cursor position all the time

" Allow backspace in insert mode
" backspace key should delete indentation, line ends, characters
" <https://gitlab.com/elisenproject/dotfiles-protesilaos/-/blob/master/vim/.vimrc>
set backspace=indent,eol,start

" hard wrap at this column
set textwidth=72

" Store lots of :cmdline history
set history=1000

set laststatus=2


" Turn off word wrap
"set nowrap
" Don't line wrap mid-word
set lbr

" Turn on omni completion:
"filetype plugin on
set omnifunc=syntaxcomplete#Complete

"====== Search
set incsearch       " jump to search match as typing
set ignorecase      " Ignore case when searching...
set hlsearch        " Highlight searches by default
set nohlsearch      " don't highlight my searches
" :noh              " No highlight
set smartcase       " Includes case in your search if you use any caps.
"============


" Complete setting
" compete acronym is cpt
:set complete+=kspell       " kspell looking in the spelling dictionary but only when spelling is enabled.
"set spell


"====== Scrolling
set scrolloff=8     "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

"====== Indentation
"set autoindent      " Uses the same indent as the line above it.
"set smartindent     " Like 'autoindent' but it recognize some code syntax and automagically indent more/less/stay the same.
set smarttab        " Tells your tab key to listen to 'shiftwidth'

"=================== Vim - Python Wiki ====================="
" https://wiki.python.org/moin/Vim
"====== use tabs to display indentation guides and remove tabs before saving file:
"
" use 4 spaces for tabs
"set tabstop=8
"set expandtab
"set softtabstop=4
"set shiftwidth=4
"filetype indent on

"============ PEP 8 Python ============
" https://realpython.com/vim-and-python-a-match-made-in-heaven/
" https://github.com/j1z0/vim-config/blob/master/vimrc
" https://superuser.com/questions/311370/solarized-colors-in-vim-dont-seem-to-be-working-for-me

filetype on
filetype indent on
filetype plugin on

" 1. Enable syntax colors
packadd! dracula
syntax enable
colorscheme dracula

" https://github.com/jnurmine/Zenburn
" To use Zenburn in Vim, you must enable the 256-color mode for Vim.
" This can be done with e.g. export TERM=xterm-256color.
" You might also need to add set t_Co=256 into your .vimrc file, before loading the colorscheme.
" 2.
set t_Co=256

"======
" https://github.com/altercation/vim-colors-solarized
" https://vi.stackexchange.com/questions/3760/togglebg-from-solarized-changes-to-incorrect-colorscheme
" 3. Solarized scheme color preferences
"let g:solarized_termcolors=256
"let g:solarized_termtrans = 1  " This gets rid of the grey background
"let g:solarized_degrade = 0
"let g:solarized_bold = 1
"let g:solarized_underline = 1
"let g:solarized_italic = 1
"let g:solarized_contrast = "normal"
"let g:solarized_visibility= "normal"

" 4. Tell vim what backgroud you are using
" Background color from color  scheme
"set bg=light
set bg=dark
"set background=light
"set background=dark
"set background=dark " or light

"if has('gui_running')
"    set background=dark
"else
"    set background=dark
"endif


" 5. and finally: Specifiy a color scheme
"colorscheme slate
"colorscheme torte
":color delek
":color desert256
"colorscheme zenburn

"colorscheme solarized



" Setting custom indent colors
" https://github.com/nathanaelkane/vim-indent-guides
"hi IndentGuidesOdd  guibg=red   ctermbg=3
"hi IndentGuidesEven guibg=green ctermbg=4

" Terminal Vim
" Settings for the indentation viewer
" When set background=dark is used, the following highlight colors will be
" defined:
hi IndentGuidesOdd  ctermbg=black
hi IndentGuidesEven ctermbg=darkgrey

"hi IndentGuidesEven ctermbg=238
"hi IndentGuidesOdd ctermbg=236

" Alternatively, when set background=light is used, the following highlight colors will be defined:
"hi IndentGuidesOdd  ctermbg=white
"hi IndentGuidesEven ctermbg=lightgrey

"======
" https://vi.stackexchange.com/questions/356/how-can-i-set-up-a-ruler-at-a-specific-column
" 'soft' limite at 80 columns and a 'hard' limit at 120 columns
" So I want a line at 80 and then every column after 120 to be colored
let &colorcolumn="72,80,".join(range(120,999),",")
hi ColorColumn ctermbg=0 guibg=lightgrey

" https://vi.stackexchange.com/questions/574/keeping-lines-to-less-than-80-characters
" set 'colorcolumn' or 'cc':
"set cc=80
"set cc=-1,+30      "set multiple columns
"highlight ColorColumn ctermbg=0 guibg=lightgrey
"======

"======

"call togglebg#map("<F5>")
"colorscheme zenburn
set guifont=Monaco:h14

"------------Start Python PEP 8 stuff----------------
" Number of spaces that a pre-existing tab is equal to.
"au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=4
au BufRead,BufNewFile *py,*pyw,*.c,*.h set tabstop=8

"spaces for indents
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*.pyw set expandtab
au BufRead,BufNewFile *.py set softtabstop=4

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.pyw match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" Wrap text after a certain number of characters
au BufRead,BufNewFile *.py,*.pyw, set textwidth=100

" Use UNIX (\n) line endings.
au BufNewFile *.py,*.pyw,*.c,*.h set fileformat=unix

" Set the default file encoding to UTF-8:
"set encoding=utf-8

" For full syntax highlighting:
let python_highlight_all=1
"syntax on

" Keep indentation level from previous line:
autocmd FileType python set autoindent

" make backspaces more powerfull
set backspace=indent,eol,start


" Folding based on indentation:
"autocmd FileType python set foldmethod=indent
" use space to open folds
"nnoremap <space> za
"----------Stop python PEP 8 stuff--------------

"js stuff"
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2

"---------- -------------- --------------

" Split Layouts
set splitbelow
set splitright

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" what is the nnoremap thing?
" In a nutshell, nnoremap remaps one key combination to another.
" The no part means remap the key in normal mode as opposed to visual mode.
" Basically, nnoremap <C-J> <C-W><C-j> says,
" in normal mode when I hit <C-J>, do <C-W><C-j> instead

" Enable folding
"set foldmethod=indent
"set foldlevel=99

" Enable folding with the spacebar
"nnoremap <space> za

"au BufNewFile,BufRead *.py
"    \ set tabstop=4
"    \ set softtabstop=4
"    \ set shiftwidth=4
"    \ set textwidth=79
"    \ set expandtab
"    \ set autoindent
"    \ set fileformat=unix
"
"au BufNewFile,BufRead *.js, *.html, *.css
"    \ set tabstop=2
"    \ set softtabstop=2
"    \ set shiftwidth=2

" Flagging Unnecessary Whitespace
"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" python with virtualenv support
"py << EOF
"import os
"import sys
"if 'VIRTUAL_ENV' in os.environ:
"  project_base_dir = os.environ['VIRTUAL_ENV']
"  activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
"  execfile(activate_this, dict(__file__=activate_this))
"EOF

"it would be nice to set tag files by the active virtualenv here
":set tags=~/mytags "tags for ctags and taglist
"omnicomplete
autocmd FileType python set omnifunc=pythoncomplete#Complete


" Syntax Checking/Highlighting
" make your code look pretty:
"let python_highlight_all=1
"syntax on

 " Color Schemes
"if has('gui_running')
"  set background=dark
"  colorscheme solarized
"else
"  colorscheme zenburn
"endif

" VIM in the Shell
" To turn it on for your shell, add the following line to ~/.inputrc:
" set editing-mode vi

"============

" Auto-remove trailing whitespace on save
autocmd BufWritePre * %s/\s\+$//e

"============ INDENTATION GUIDES
" invisible characters to display (with :set list)
set listchars=tab:›\ ,nbsp:␣,trail:•,precedes:«,extends:»
""""set listchars=tab:›\ ,space:·,nbsp:␣,trail:•,eol:¬,precedes:«,extends:»

" display indentation guides
"set list listchars=tab:❘-,trail:·,extends:»,precedes:«,nbsp:×
"set list listchars=tab:\❘-\,trail:·,extends:»,precedes:«,nbsp:×

" Display tabs and trailing spaces visually
"set list listchars=tab:\ \ ,trail:·
"set list listchars=tab:\|-\ ,trail:·

" https://askubuntu.com/questions/74485/how-to-display-hidden-characters-in-vim
"set list
"set listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»

"======
"set nolist
"set list       " show hidden characters in Vim
"set listchars=tab:\❘-\,trail:·,extends:»,precedes:«,nbsp:×
"set list listchars=tab:\ \ ,trail:·,extends:»,precedes:«,nbsp:×
"set listchars=tab:\❘-\,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»
" convert spaces to tabs when reading file
"autocmd! bufreadpost * set noexpandtab | retab! 4

" convert tabs to spaces before writing file
"autocmd! bufwritepre * set expandtab | retab! 4

" convert spaces to tabs after writing file (to show guides again)
"autocmd! bufwritepost * set noexpandtab | retab! 4
"======

" :h listchars
" :set nolist   " hide hidder chars
"set list        " show hidden characters in Vim
" Settings for hidden chars
" what particular chars they are displayed with
"set listchars=tab:>-,trail:-,eol:$,nbsp:%,extends:>,precedes:<
"set listchars=tab:>-,trail:-,nbsp:%,extends:>,precedes:<

" Add indentation guides/lines
" https://vi.stackexchange.com/questions/666/how-to-add-indentation-guides-lines
"set cursorcolumn
"set cursorline

"======

"======
let mapleader=","
"======

"======
" Window Vim pane resizing
" https://coderwall.com/p/r-_mkw/vim-panes-resizing
" maximize panes:
" - Horizontally with <kbd>Ctrl w |</kbd>
" - Vertically with <kbd>Ctrl w_</kbd>
" To make all panes equal type <kbd>Ctrl w =</kbd>. As resizing is a frequent
" action, you can condider following mappings:
nnoremap <silent><Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent><Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>
nnoremap <silent><Leader>> :exe "resize " . (winwidth(0) * 3/2)<CR>
nnoremap <silent><Leader>< :exe "resize " . (winwidth(0) * 2/3)<CR>
"======

"======
" Because node_modules shouldn't be ctrlp'd
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
"======

"====== RUNNING SCRIPT CODE IN VIM
" https://stackoverflow.com/questions/18948491/running-python-code-in-vim
" Step by step explanation:
" ! allows you to run a terminal command
" clear will empty your terminal screen
" ; ends the first command, allowing you to introduce a second command
" python will use python to run your script (it could be replaced with ruby for example)
" % concats the current filename, passing it as a parameter to the python command
"
" :! clear; python %
"
" Run the last command:
" :!!

" see the code while looking at its' output you could find :ter (:terminal) command
" Using vert in the second line runs the code in vertical split instead of horizontal.
" close the split-window where the code ran
" :vert ter python3 %
" :close
"======

"======
" press <F9> to execute the current buffer with python
"autocmd FileType python map <buffer> <F9> :w<CR>:exec '!python3' shellescape(@%, 1)<CR>
"autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!python3' shellescape(@%, 1)<CR>

" Explanation:
"
" autocmd: command that Vim will execute automatically on {event} (here: if you open a python file)
" [i]map: creates a keyboard shortcut to <F9> in insert/normal mode
" <buffer>: If multiple buffers/files are open: just use the active one
" <esc>: leaving insert mode
" :w<CR>: saves your file
" !: runs the following command in your shell (try :!ls)
" %: is replaced by the filename of your active buffer. But since it can contain things like whitespace and other "bad" stuff it is better practise not to write :python %, but use:
" shellescape: escape the special characters. The 1 means with a backslash
" TL;DR: The first line will work in normal mode and once you press <F9> it first saves your file and then run the file with python. The second does the same thing, but leaves insert mode first
"======
" saves the current buffer and execute the code with presing only Esc + F9
"map <F9> :w<CR>:!python %<CR>"
"======
" extends for any language with 1 keybinding with augroup command:
"augroup rungroup
"   autocmd!
"   autocmd BufRead,BufNewFile *.go nnoremap <F5> :exec '!go run' shellescape(@%, 1)<cr>
"   autocmd BufRead,BufNewFile *.py nnoremap <F5> :exec '!python' shellescape(@%, 1)<cr>
"augroup END
"======
" Automatically set paste mode in Vim when pasting in insert mode
" https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
" https://stackoverflow.com/questions/2514445/turning-off-auto-indent-when-pasting-text-into-vim/38258720#38258720
" :r! cat and then paste ( shift + insert ) the content, and CTRL+D.
" No need to enable & disable, direct usage.

" https://stackoverflow.com/questions/2514445/turning-off-auto-indent-when-pasting-text-into-vim/38258720#38258720
" To turn off autoindent when you paste code, there's a special "paste" mode.
" Type
" :set paste
" Then paste your code. Note that the text in the tooltip now says -- INSERT (paste) --.
" After you pasted your code, turn off the paste-mode, so that auto-indenting when you type works correctly again.
" :set nopaste
" map <F3> or <F2> such that it can switch between paste and nopaste modes while editing the text!
"set pastetoggle=<F3>
"set pastetoggle=<F2>
"======

"====== SNIPPETS
" SNIPPETS:
" Read an empty python3 template and move cursor to write the code
nnoremap ,py :-1read $HOME/.vim/.template.py<CR>8jo

" SNIPPETS:
" Read an empty python3 template and move cursor to write the code
nnoremap ,sh :-1read $HOME/.vim/.script.sh<CR>jo
"======

"===== SPELL CHECK VIM
" :setlocal spell spelllang=fr
" :setlocal spell spelllang=en
" :set nospell
"
"====  Spell check set to F6:
map <F6> :setlocal spell! spelllang=fr<CR>
map <F7> :set spelllang=en<CR>
"=================== ELISE CONFIGURATION END ====================="


"=================== no_plugins.vim ====================="

" https://github.com/changemewtf/no_plugins/blob/master/no_plugins.vim

" HOW TO DO 90% OF WHAT PLUGINS DO(WITH JUST VIM) -Max Cantor

" FEATURES TO COVER:
" - Fuzzy File Search
" - Tag jumping
" - Autocomplete
" - File Browsing
" - Snippets
" - Build Integration(if we have time)


" BASIC SETUP:

" enter the current millenium
"set nocompatible

" enable syntax and plugins(for netrw)
"syntax enable
"filetype plugin on

" https://www.tutorialdocs.com/article/vim-configuration.html
" Turn on syntax highlighting. And it can identification code automatically,
" and be displayed in multiple colors.
set showmode

" FINDING FILES:

" Search down into subfolders
" Provides tab-competion for all file-related tasks
set path+=**

" Display all matching files when we tab complete
set wildmenu

" NOW WE CAN:
" - Hit tab to :find by partial match
" - Use * to make it fuzzy

" THINGS TO CONSIER:
" - :b lets you autocomplete any open buffer


" TAG JUMPING:
" Create the 'tags' file(may need to install ctags first)
command! MakeTags !ctags -R .

" NOW WE CAN:
" - Use ^] to jump to tag under cursor
" - Use g^] for ambiguous tags
" - Use ^t to jump back the tag stack

" THINGS TO CONSIDER:
" - This doesn't help if you want a visual list of tags


" AUTOCOMPLETE:
" The good stuff is documented in |ins-completion|

" HIGHLIGHTS:
" - ^x^n for JUST this file
" - ^x^f for filenames(works with our path trick!)
" - ^x^] for tags only
" - ^n for anything specified by the 'complete' option

" NOW WE CAN:
" - Use ^n and ^p to go back and forth in the suggestion List


" FILE BROWSING:

" Tweaks for browing
let g:netrw_banner=0            " disable annoying banner
let g:netrw_browse_split=4      " open in prior window
let g:netrw_altv=1              " open splits to the right
let g:netrw_liststyle=3         " tree view
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'

let g:netrw_winsize = 25

"augroup ProjectDrawer
"    autocmd!
"    autocmd VimEnter * :Vexplore
"augroup END

" NOW WE CAN:
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings


" SNIPPETS:
" Read an empty HTML template and move cursor to title
nnoremap ,html :-1read $HOME/.vim/.skeleton.html<CR>4jwf>a

" NOW WE CAN:
" - Take over the world!
"   (with much fewer keystrokes)


" BUILD INTEGRATION:

" Steal Mr. Bradley's formatter & add it to our spec_helper
" https://www.philipbradley.net/posts/rspec-into-vim-with-quickfix/

" Configure the `make` command to run RSpec
set makeprg=bundle\ exec\ rspec\ -f\ QuickfixFormatter

" NOW WE CAN:
" - Run :make to run RSpec
" - :cl to list errors
" - :cc# to jump to error by number
" - :cn and :cp to navigate forward and back
